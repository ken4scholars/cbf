import re

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.deconstruct import deconstructible
from django.utils.translation import ungettext


@deconstructible
class PhoneValidator(RegexValidator):
    # regex = re.compile(r'^\+?1?\d{9,15}$', re.IGNORECASE)
    regex = re.compile(r'^(\+7|7|8)+([0-9]){10}$')
    message = 'Введите корректный номер телефона.'

validate_phone = PhoneValidator()


class PasswordValidator(object):
    """
    Validate whether the password is of a minimum length.
    """
    def __init__(self, min_length=6):
        self.min_length = min_length

    def validate(self, password, user=None):
        if len(password) < self.min_length:
            raise ValidationError(
                ungettext(
                    "Пароль должен содержить хотя бы %(min_length)d знаков.",
                    "Пароль должен содержить хотя бы %(min_length)d знаков.",
                    self.min_length
                ),
                code='password_too_short',
                params={'min_length': self.min_length},
            )

    def get_help_text(self):
        return ungettext(
            "Пароль должен содержить хотя бы %(min_length)d знаков",
            "Пароль должен содержить хотя бы %(min_length)d знаков",
            self.min_length
        ) % {'min_length': self.min_length}



