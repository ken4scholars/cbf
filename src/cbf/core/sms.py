import logging
from django.conf import settings

from cbf.contrib.smsaero import SmsAero, SmsAeroError


logger = logging.getLogger(__name__)


class SMS:

    def __init__(self):
        self.user = settings.SMSAERO_USER
        self.password = settings.SMSAERO_PASSWORD
        self.url_gate = getattr(settings, 'SMSAERO_URL_GATE', SmsAero.URL_GATE)
        self.signature = getattr(settings, 'SMSAERO_SIGRANTURE', SmsAero.SIGNATURE)
        self.digital = getattr(settings, 'SMSAERO_DIGITAl', SmsAero.DIGITAL)
        self.type_send = getattr(settings, 'SMSAERO_TYPE_SEND', SmsAero.TYPE_SEND)

        self.api = SmsAero(user=self.user, passwd=self.password,
                           url_gate=self.url_gate, signature=self.signature,
                           digital=self.digital, type_send=self.type_send)

    def send(self, phone, message):
            logger.info('sending SMS to: {}'.format(phone))
        # try:
            return self.api.send(to=phone, text=message, selector='send')
        #     print(response)
        #     if response['result'] == 'accepted':
        #         return True
        # except SmsAeroError as e:
        #     print(e)
        #     return False


sms = SMS()


def send_code(phone, code):

    message = "{} Введите этот код для подтверждения регистрации.".format(code)
    return sms.send(phone, message)
