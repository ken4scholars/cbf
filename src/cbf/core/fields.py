import json
from django.contrib.postgres.forms.jsonb import InvalidJSONInput, JSONField
from django.core.exceptions import ValidationError
from django.utils import formats
from django.utils.encoding import force_text
from rest_framework.fields import CharField
from rest_framework.fields import empty
from cbf.core.validators import PhoneValidator
from django.forms.fields import IntegerField


class PhoneField(CharField):
    default_error_messages = {
        'invalid': "Введите корректный номер телефона.",
    }

    def __init__(self, **kwargs):
        super(PhoneField, self).__init__(**kwargs)
        # self.normalize()
        # print(self.initial)
        validator = PhoneValidator(message=self.error_messages['invalid'])
        self.validators.append(validator)

    # def to_internal_value(self, data):
    #     return data[-10:]

    def run_validation(self, data=empty):
        if data and isinstance(data, str):
            data = data.replace(' ', '')
            data = data.replace('-', '')
            data = data.replace('(', '')
            data = data.replace(')', '')
        (is_empty_value, data) = self.validate_empty_values(data)
        if is_empty_value:
            return data
        self.run_validators(data)
        value = self.to_internal_value(data)
        return value

    def to_internal_value(self, data):
        return "+7{}".format(data[-10:])

    def normalize(self):
        phone = self.initial
        if phone and isinstance(phone, str):
            phone = phone.replace(' ', '')
            phone = phone.replace('-', '')
            phone = phone.replace('(', '')
            phone = phone.replace(')', '')
        self.initial = phone


class BigIntegerField(IntegerField):
    def to_python(self, value):
        """
        Validates that int() can be called on the input. Returns the result
        of int(). Returns None for empty values.
        """
        value = int(float(value))
        if value in self.empty_values:
            return None
        if self.localize:
            value = formats.sanitize_separators(value)
        # Strip trailing decimal and zeros.
        try:
            value = int(self.re_decimal.sub('', force_text(value)))
        except (ValueError, TypeError):
            raise ValidationError(self.error_messages['invalid'], code='invalid')
        return value


class UTF8JSONFormField(JSONField):
    def prepare_value(self, value):
        if isinstance(value, InvalidJSONInput):
            return value
        return json.dumps(value, ensure_ascii=False, indent=4)