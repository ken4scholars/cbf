from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _

from .models import Partner


class PartnerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Partner
        fields = '__all__'
