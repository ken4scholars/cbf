import hashlib


def get_cache_key(code, prefix):
    hash_value = code

    return '{prefix}:{token}'.format(
        prefix=prefix,
        token=hashlib.md5(hash_value.encode('UTF-8')).hexdigest()
    )
