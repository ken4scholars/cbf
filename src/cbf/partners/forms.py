from django import forms
from django.contrib.auth import forms as auth_forms, get_user_model
from django.utils.translation import ugettext_lazy as _

from cbf.core.validators import PasswordValidator, validate_phone
from cbf.core.utils import get_object_or_none
from cbf.core.fields import BigIntegerField
from .models import Partner

password_validator = PasswordValidator()

UserModel = get_user_model()


def normalize_phone(phone):
    phone = phone.replace(' ', '')
    phone = phone.replace('-', '')
    phone = phone.replace('(', '')
    phone = phone.replace(')', '')
    return phone


class EmailForm(forms.Form):
    email = forms.EmailField(label=_('Email'),
                             required=True,
                             widget=forms.EmailInput(attrs={'placeholder': 'Email',
                                                            'class': 'form-control'
                                                            }))


class LoginForm(EmailForm):

    password = forms.CharField(label=_('Пароль'),
                               widget=forms.PasswordInput(attrs={'placeholder': 'Пароль(от 6 символов)',
                                                                 'class': 'form-control'
                                                                 }),
                               required=True)

    def clean_password(self):
        password = self.cleaned_data.get('password')
        auth_forms.password_validation.validate_password(password,
                                                         password_validators=[password_validator])
        return password


class RegistrationForm(LoginForm):
    # auth_forms.PasswordChangeForm

    phone = forms.CharField(label=_('Телефон'),
                            widget=forms.TextInput(attrs={'placeholder': 'Телефон',
                                                          'class': 'form-control phone'
                                                          }),
                            required=True)
    password2 = forms.CharField(label=_('Пароль (Подтверждение)'),
                                widget=forms.PasswordInput(attrs={'placeholder': 'Пароль (Подтверждение)',
                                                                  'class': 'form-control'
                                                                  }),
                                required=True)

    def clean_password2(self):
        password2 = self.cleaned_data.get('password2')
        if password2 != self.cleaned_data.get('password'):
            raise forms.ValidationError('Пароли не совпадают.', 'invalid')
        return password2

    def clean_email(self):
        email = self.cleaned_data.get('email')
        user = get_object_or_none(UserModel, email=email)
        if user:
            raise forms.ValidationError('Почта принадлежит другой учетной записи', 'invalid')
        return email

    def clean_phone(self):
        phone = self.cleaned_data.get('phone')
        phone = normalize_phone(phone)
        user = get_object_or_none(UserModel, phone=phone)
        if user:
            raise forms.ValidationError('Телефон принадлежит другой учетной записи', 'invalid')
        return phone


class ChangePasswordForm(RegistrationForm):
    email = None
    phone = None

    password = forms.CharField(label=_('Новый пароль'),
                               required=True,
                               widget=forms.PasswordInput(attrs={'placeholder': 'Новый пароль(от 6 символов)',
                                                                 'class': 'form-control'
                                                                 }))


class SetupForm(forms.ModelForm):

    name = forms.CharField(label=_('Название компаниии'),
                           required=True,
                           widget=forms.TextInput(attrs={'placeholder': 'Ваша Компания',
                                                         'class': 'form-control'
                                                         }),
                           )
    city = forms.CharField(label=_('Город для отображения в каталоге'),
                           required=True,
                           widget=forms.TextInput(attrs={
                                                         # 'placeholder': 'Казань',
                                                         'class': 'form-control',
                                                         'data-role': 'typeahead',
                                                         }),
                           )

    percent = forms.IntegerField(label=_('Процент кешбека'),
                                 required=True,
                                 min_value=0,
                                 max_value=100,
                                 widget=forms.NumberInput(attrs={'placeholder': '10',
                                                                 'class': 'form-control',
                                                                 }))
    website = forms.URLField(label=_('Сайт'),
                             required=False,
                             widget=forms.TextInput(attrs={'placeholder': 'Адрес сайта (необязательно)',
                                                           'class': 'form-control'
                                                           }))

    products = forms.CharField(label=_('Что вы предлагаете? Введите список категорий товаров или услуг которые вы хотите разм'),
                               required=True,
                               widget=forms.TextInput(attrs={
                                                             # 'placeholder': 'Категория продуктов через запятую',
                                                             'class': 'form-control',
                                                             'data-role': 'typeahead',
                                                             }))

    class Meta:
        model = Partner
        fields = ['name', 'city', 'percent', 'website', 'products']


class AddressForm(forms.Form):
    address = forms.CharField(label=_('Адрес(улица номер дома...)'),
                              required=True,
                              widget=forms.TextInput(attrs={'placeholder': 'Адрес(улица номер дома...)',
                                                            'class': 'form-control',
                                                            }))
    phone = forms.CharField(label=_('Телефон'),
                            required=False,
                            widget=forms.TextInput(attrs={'placeholder': 'Телефон',
                                                          'class': 'form-control phone'
                                                          }))
    work_hours = forms.CharField(label=_('Режим работы (18-21 без выходных)'),
                                 required=False,
                                 widget=forms.TextInput(attrs={'placeholder': 'Режим работы (18-21 без выходных)',
                                                               'class': 'form-control',
                                                               }))

AddressFormSet = forms.formset_factory(AddressForm)


class PaymentDataForm(forms.Form):
    required_css_class = 'required'

    LEGAL, PHYSICAL = 'OOO', 'IP'
    OWNERSHIP_TYPE = (
        (LEGAL, _('ООО')),
        (PHYSICAL, _('ИП')),
    )

    ownership = forms.ChoiceField(label=_('Тип собственности'),
                                  choices=OWNERSHIP_TYPE,
                                  widget=forms.Select(attrs={'class': 'form-control'
                                                             }))

    inn = BigIntegerField(label=_('ИНН'),
                          required=True,
                          min_value=1000000000,
                          max_value=999999999999,
                          widget=forms.NumberInput(attrs={'placeholder': '10/12 цифр',
                                                          'class': 'form-control'
                                                          }))

    kpp = BigIntegerField(label=_('КПП'),
                          required=True,
                          min_value=100000000,
                          max_value=999999999,
                          widget=forms.NumberInput(attrs={'placeholder': '9 цифр',
                                                          'class': 'form-control'
                                                          }))

    ogrn_ogrnip = BigIntegerField(label=_('ОГРН/ОГРНИП'),
                                  required=True,
                                  min_value=1000000000000,
                                  max_value=999999999999999,
                                  widget=forms.NumberInput(attrs={'placeholder': '13/15 цифр',
                                                                  'class': 'form-control'
                                                                  }))

    reg_date = forms.DateField(label=_('Дата внесения записи о регистрации в ЕГРЮЛ/ЕГРИП'),
                               required=True,
                               widget=forms.DateInput(attrs={'class': 'form-control'}))

    fio = forms.CharField(label=_('ФИО подписанта'),
                          required=True,
                          min_length=5,
                          max_length=200,
                          widget=forms.TextInput(attrs={'placeholder': 'ФИО подписанта',
                                                        'class': 'form-control'
                                                        }))

    def clean_ogrn_ogrnip(self):
        number = self.cleaned_data.get('ogrn_ogrnip')
        if len(str(number)) != 13 and len(str(number)) != 15:
            raise forms.ValidationError('ОГРН/ОГРНИП должен содержить 13 или 15 цифр', 'invalid')
        return int(number)

    def clean_inn(self):
        number = self.cleaned_data.get('inn')
        if len(str(number)) != 10 and len(str(number)) != 12:
            raise forms.ValidationError('ИНН должен содержить 10 или 12 цифр', 'invalid')
        return int(number)


class OrgContactForm(forms.Form):
    required_css_class = 'required'

    phone = forms.CharField(label=_('Телефон'),
                            required=False,
                            widget=forms.TextInput(attrs={'placeholder': 'Телефон',
                                                          'class': 'form-control phone'
                                                          }))

    fax = forms.CharField(label=_('Факс'),
                          required=False,
                          widget=forms.TextInput(attrs={'placeholder': 'Факс',
                                                        'class': 'form-control phone'
                                                        }))

    email = forms.EmailField(label=_('Email'),
                             required=True,
                             widget=forms.EmailInput(attrs={'placeholder': 'Email бухгалтерии, для отчетов',
                                                            'class': 'form-control'
                                                            }))


class AccountForm(forms.Form):
    required_css_class = 'required'

    account_name = forms.CharField(label=_('Наименование'),
                                   widget=forms.TextInput(attrs={'placeholder': 'Мой счет',
                                                                 'class': 'form-control'
                                                                 }))

    bik = BigIntegerField(label=_('БИК'),
                          required=True,
                          min_value=100000000,
                          max_value=999999999,
                          widget=forms.NumberInput(attrs={'placeholder': '9 цифр',
                                                          'class': 'form-control'
                                                          }))

    korr_account = BigIntegerField(label=_('Корр. счёт'),
                                   required=True,
                                   min_value=10000000000000000000,
                                   max_value=99999999999999999999,
                                   widget=forms.NumberInput(attrs={'placeholder': '20 цифр',
                                                                   'class': 'form-control'
                                                                   }))

    rasc_account = BigIntegerField(label=_('Расч. счёт'),
                                   required=True,
                                   min_value=10000000000000000000,
                                   max_value=99999999999999999999,
                                   widget=forms.NumberInput(attrs={'placeholder': '20 цифр',
                                                                   'class': 'form-control',
                                                                   }))


class LegalAddressForm(forms.Form):
    required_css_class = 'required'

    index = forms.IntegerField(label=_('Индекс'),
                               required=True,
                               widget=forms.NumberInput(attrs={'placeholder': 'Индекс',
                                                               'class': 'form-control',
                                                               }))

    country = forms.CharField(label=_('Страна'),
                              required=True,
                              widget=forms.TextInput(attrs={'placeholder': 'РОССИЯ',
                                                            'class': 'form-control'
                                                            }))

    locality = forms.CharField(label=_('Населеный пункт'),
                               required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Населеный пункт',
                                                             'class': 'form-control'
                                                             }))

    street = forms.CharField(label=_('Улица'),
                             required=True,
                             widget=forms.TextInput(attrs={'placeholder': 'Улица',
                                                           'class': 'form-control'
                                                           }))

    house = forms.CharField(label=_('Дом'),
                            required=True,
                            widget=forms.TextInput(attrs={'placeholder': 'Дом',
                                                          'class': 'form-control'
                                                          }))

    office = forms.CharField(label=_('Офис'),
                             required=True,
                             widget=forms.TextInput(attrs={'placeholder': 'Офис',
                                                           'class': 'form-control'
                                                           }))

