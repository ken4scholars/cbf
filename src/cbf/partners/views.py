import json
import logging
import uuid
import os
import sendgrid
from reportlab.pdfbase.ttfonts import TTFont
from sendgrid.helpers.mail import *
from reportlab.pdfbase import pdfmetrics
from io import BytesIO
from reportlab.pdfgen import canvas
from django import forms as django_forms
from django.utils import timezone
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.shortcuts import render, Http404, redirect, resolve_url
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.http import HttpResponse
from django.conf import settings
from django.core.cache import cache
from django.views.generic import View, TemplateView
from django.utils.decorators import method_decorator
from django.contrib import auth as user_auth, messages
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.contrib.auth.views import LoginView, SuccessURLAllowedHostsMixin
# from django.contrib.auth.decorators import login_required
from django.views.decorators import http, csrf
from django.contrib.auth.backends import ModelBackend

from cbf.core.utils import get_object_or_none
from cbf.users.models import PARTNER
from .utils import get_cache_key
from . import forms
from .models import Partner, PartnerPayment

logger = logging.getLogger(__name__)

UserModel = user_auth.get_user_model()
# login_form = forms.LoginForm()
# register_form = forms.RegistrationForm()
change_password_prefix = 'partners.change_password'
sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_APIKEY'))
# logger.info(os.environ.get('SENDGRIP_APIKEY'))

# next_url = None


class AuthBackend(ModelBackend):

    def authenticate(self, request, email=None, password=None, **kwargs):
        if email is None:
            email = kwargs.get('email')
        user = get_object_or_none(UserModel, email=email)
        if user and user.check_password(password) and self.user_can_authenticate(user):
            return user
        return None


class BaseView(View):
    def get(self, request, *args, **kwargs):
        return redirect('/partners/control-panel')


@method_decorator(csrf.csrf_exempt, name='dispatch')
@method_decorator(never_cache, name='dispatch')
class LoginView(View, SuccessURLAllowedHostsMixin):
    redirect_field_name = 'next'
    # next_url = None

    def get(self, request):
        next_url = self.get_success_url()
        if request.user and hasattr(request.user, 'partner'):
            if not request.user.partner.setup:
                return redirect('/partners/setup')
            if next_url:
                return redirect(next_url)
            # messages.error(request, 'Нужно выйти из текущей учетной записи прежде чем авторизоватся еще раз')
            return redirect('/partners/control-panel')
        return render(request, 'login.html', {'login_form': forms.LoginForm(),
                                              'next': next_url,
                                              'title': 'CBF Partners| Авторизация'
                                              })

    def post(self, request, *args, **kwargs):
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            user = AuthBackend().authenticate(request, email=form.cleaned_data['email'],
                                              password=form.cleaned_data['password'])
            if user and hasattr(user, 'partner'):
                user_auth.logout(request)
                user_auth.login(request, user)
                if not user.partner.setup:
                    return redirect('/partners/setup')
                next_url = self.get_success_url()
                if next_url:
                    return redirect(next_url)
                return redirect('/partners/control-panel')
            elif user:
                form.add_error(None, 'Пожалуйста, авторизируйтесь как партнер.')
            else:
                form.add_error(None, 'Либо пароль неправильный либо почта.')
        next_url = self.request.POST.get(
            self.redirect_field_name,
            self.request.GET.get(self.redirect_field_name, None)
        )
        return render(request, 'login.html', {'login_form': form,
                                              'next': next_url,
                                              'title': 'CBF Партенры | Авторизация'
                                              })

    def get_success_url(self):
        """Ensure the user-originating redirection URL is safe."""
        redirect_to = self.request.POST.get(
            self.redirect_field_name,
            self.request.GET.get(self.redirect_field_name, None)
        )
        if not redirect_to:
            return None
        url_is_safe = is_safe_url(
            url=redirect_to,
            allowed_hosts=self.get_success_url_allowed_hosts(),
            require_https=self.request.is_secure(),
        )
        if not url_is_safe:
            return resolve_url(settings.LOGIN_REDIRECT_URL)
        return redirect_to


@method_decorator(csrf.csrf_exempt, name='dispatch')
class RegisterView(View):

    def get(self, request):
        return render(request, 'register.html', {'register_form': forms.RegistrationForm(),
                                                 'title': 'CBF Партенры | Регистрация'
                                                  })

    def post(self, request, *args, **kwargs):
        form = forms.RegistrationForm(request.POST)
        if form.is_valid():
            user = UserModel.objects.create(
                phone=form.cleaned_data.get('phone'),
                email=form.cleaned_data.get('email'),
                role=PARTNER
            )
            user.set_password(form.cleaned_data.get('password'))
            user.save()
            Partner.objects.create(
                phone=form.cleaned_data.get('phone'),
                email=form.cleaned_data.get('email'),
                user=user,
                setup=False
            )
            user_auth.logout(request)
            user_auth.login(request, user)
            return redirect('/partners/setup')
        return render(request, 'register.html', {'register_form': form,
                                                 'title': 'CBF Партенры | Регистрация'
                                                 })


def is_partner(user, form=None):
    if not hasattr(user, 'partner'):
        if form:
            form.add_error(None, 'Пожалуйста, авторизируйтесь как партнер.')
        return False
    return True


class ForgotPasswordView(View):
    template_name = 'forgot_password.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'form': forms.EmailForm(),
                                                    'title': 'CBF Партенры | Восстановление пароля'
                                                    })

    def post(self, request, *args, **kwargs):
        form = forms.EmailForm(request.POST)
        if form.is_valid():
            user = get_object_or_none(UserModel, email=form.cleaned_data.get('email'))
            if user:
                if is_partner(user, form):
                    code = str(uuid.uuid4())
                    website = settings.WEBSITE
                    cache_key = get_cache_key(code, change_password_prefix)
                    cache.set(cache_key, user.email, settings.VERIFICATION_CODE_MAX_AGE)
                    message = render_to_string('change_password_email.html', {
                        'link': website + '/partners/' + code + '/change-pass',
                        'website': website,
                        'duration': round(settings.VERIFICATION_CODE_MAX_AGE / 60)
                    })
                    email = Mail(
                        from_email=Email(settings.DEFAULT_FROM_EMAIL),
                        subject='Восстановления пароля',
                        to_email=Email(user.email),
                        content=Content('text/html', message)
                    )
                    try:
                        response = sg.client.mail.send.post(request_body=email.get())
                        logger.info('Сообщение на восстановление пароля отправлен в {} с ответом {} и статусом {}'
                                    .format(user.email, response.body, response.status_code))
                        messages.success(request, 'Ссылка для восстановления пароля отправлена в почту.')
                    except Exception as e:
                        logger.error('Ошибка при отправление сообщения на почту {}'.format(e))
                        messages.error(request, 'Не удалось отправить сообщение на почту. Пропробуйте еще раз попозже.')
            else:
                messages.error(request, 'Не найдена учетная запись с такой почтой, проверите и отправьте еще раз.')
        return render(request, self.template_name, {'form': form,
                                                    'title': 'CBF Партенры | Восстановление пароля'
                                                    })


class ChangePasswordView(View):
    template_name = 'change_password.html'

    def get(self, request, code, *args, **kwargs):
        cache_key = get_cache_key(code, change_password_prefix)
        user = get_object_or_none(UserModel, email=cache.get(cache_key))
        cache.delete(cache_key)
        if not user:
            messages.error(request, 'Cсылка не правильная или устарела.')
            return redirect('/partners/forgot-pass')
        return render(request, self.template_name, {'form': forms.ChangePasswordForm(),
                                                    'code': code,
                                                    'email': user.email,
                                                    'title': 'CBF Партенры | Восстановление пароля',
                                                    })

    def post(self, request, code, *args, **kwargs):
        form = forms.ChangePasswordForm(request.POST)
        user = get_object_or_none(UserModel, email=request.GET.get('email', ''))
        if form.is_valid():

            if not user or not hasattr(user, 'partner'):
                messages.error(request, 'Извините, Что-то пошло не так, повторите еще раз!')
                return redirect('/partners/forgot-pass')
            user.set_password(form.cleaned_data.get('password'))
            user.save()
            messages.success(request, 'Новый пароль успешно установлен!')
            return redirect('/partners/login')
        return render(request, self.template_name, {'form': form,
                                                    'code': code,
                                                    'email': user.email,
                                                    'title': 'CBF Партенры | Восстановление пароля',
                                                    })


@method_decorator(user_auth.decorators.login_required, name='dispatch')
class SetupView(View):

    template_name = 'setup.html'

    def get(self, request, *args, **kwargs):
        if not hasattr(request.user, 'partner'):
            messages.error(request, 'Пожалуйста, авторизируйтесь как партнер чтобы продолжить.')
            return redirect('/partners/login?next=/partners/setup')
        form = forms.SetupForm(instance=request.user.partner)
        formset = forms.AddressFormSet()
        if request.user.partner.address != {}:
            address_formset = django_forms.formset_factory(forms.AddressForm, extra=0)
            formset = address_formset(initial=request.user.partner.address)
        return render(request, self.template_name,
                      {'setup_form': form,
                       'address_formset': formset,
                       'title': 'CBF Партенры | Ввод данных для активации'
                       })

    def post(self, request, *args, **kwargs):
        form = forms.SetupForm(request.POST)
        formset = forms.AddressFormSet(request.POST)
        error = False
        if not is_partner(request.user, form):
            error = True
        else:
            form = forms.SetupForm(request.POST, instance=request.user.partner)
            if form.is_valid() and formset.is_valid():
                form.save(commit=False)
                partner = request.user.partner
                address = []
                for form in formset:
                    if form.cleaned_data.get('address'):
                        address.append(dict({
                            'address': form.cleaned_data.get('address'),
                            'phone': form.cleaned_data.get('phone'),
                            'work_hours': form.cleaned_data.get('work_hours'),
                        }))
                partner.address = address
                partner.setup = True
                partner.save()
            else:
                error = True
        if error:
            return render(request, self.template_name, {'setup_form': form,
                                                        'address_formset': formset,
                                                        'title': 'CBF Партенры | Регистрация'
                                                        })
        return redirect('/partners/control-panel')


@method_decorator(user_auth.decorators.login_required, name='dispatch')
class ControlPanelView(View):

    def get(self, request, *args, **kwargs):
        if not is_partner(request.user):
            messages.error(request, 'Пожалуйста, авторизируйтесь как партнер чтобы продолжить.')
            return redirect('/partners/login?next=/partners/control-panel')
        payments = PartnerPayment.objects.filter(partner=request.user.partner,
                                                 created__gte=timezone.now() - timezone.timedelta(days=30))
        month_sales = sum([payment.sum for payment in payments if payment.status != PartnerPayment.CANCELED])
        unpaid = sum([payment.sum for payment in PartnerPayment.objects.filter(partner=request.user.partner)
                      if payment.status == PartnerPayment.CREATED])
        return render(request, 'control_panel.html', {'partner': request.user.partner,
                                                      'month_sales': month_sales,
                                                      'unpaid': unpaid,
                                                      'website': settings.WEBSITE,
                                                      'title': 'CBF Партенры | Панель управления'
                                                      })


@method_decorator(user_auth.decorators.login_required, name='dispatch')
class DownloadInvoiceView(View):

    def get(self, request, partner_id, invoice_number, *args, **kwargs):
        if not is_partner(request.user):
            messages.error(request, 'Пожалуйста, авторизируйтесь как партнер чтобы продолжить.')
            return redirect('/partners/login?next=/partners/{}/payment/{}'.format(partner_id, invoice_number))
        partner = get_object_or_none(Partner, id=partner_id)
        if not partner:
            raise Http404
        if partner != request.user.partner:
            messages.error(request, 'Вы не имеете права на просмотра этой страницы.')
            return render(request, 'messages.html')
        payment = get_object_or_none(PartnerPayment, invoice_number=invoice_number)
        if not payment:
            return Http404

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="payment-{}.pdf"'.format(invoice_number)

        response.write(self.generate_invoice(payment))
        return response

    def generate_invoice(self, payment):
        buffer = BytesIO()
        text = 'Номер счета: {}\n Сумма: {}'.format(payment.invoice_number, payment.sum)
        pdfmetrics.registerFont(TTFont('FreeSans', settings.STATIC_ROOT + '/fonts/FreeSans.ttf'))
        p = canvas.Canvas(buffer)
        p.setFont("FreeSans", 14)
        p.drawString(1, 1, text)
        p.showPage()
        p.save()

        pdf = buffer.getvalue()
        buffer.close()
        return pdf


@method_decorator(user_auth.decorators.login_required, name='dispatch')
class PaymentDataView(View):
    template_name = 'payment_data.html'

    def get(self, request, *args, **kwargs):
        if not is_partner(request.user):
            messages.error(request, 'Пожалуйста, авторизируйтесь как партнер чтобы продолжить.')
            return redirect('/partners/login?next=/partners/payment-data')
        payment_data_form = forms.PaymentDataForm()
        contact_form = forms.OrgContactForm()
        account_form = forms.AccountForm()
        legal_address_form = forms.LegalAddressForm()
        return render(request, self.template_name, {'payment_data_form': payment_data_form,
                                                    'account_form': account_form,
                                                    'contact_form': contact_form,
                                                    'legal_address_form': legal_address_form,
                                                    'title': 'CBF Партенры | Данные для договора'
                                                    })

    def post(self, request, *args, **kwargs):
        if not is_partner(request.user):
            messages.error(request, 'Пожалуйста, авторизируйтесь как партнер чтобы продолжить.')
            return redirect('/partners/login?next=/partners/payment-data')
        payment_data_form = forms.PaymentDataForm(request.POST)
        contact_form = forms.OrgContactForm(request.POST)
        account_form = forms.AccountForm(request.POST)
        legal_address_form = forms.LegalAddressForm(request.POST)
        if payment_data_form.is_valid() and contact_form.is_valid() and \
                account_form.is_valid() and legal_address_form.is_valid():
            payment_data = payment_data_form.cleaned_data
            payment_data['contacts'] = contact_form.cleaned_data
            payment_data['account'] = account_form.cleaned_data
            payment_data['legal_address'] = legal_address_form.cleaned_data
            payment_data['reg_date'] = payment_data['reg_date'].isoformat()
            request.user.partner.payment_data = payment_data
            request.user.partner.save()
            messages.success(request, 'Данные для договора успешно сохранены')
            return redirect('/partners/sales')
        return render(request, self.template_name, {'payment_data_form': payment_data_form,
                                                    'account_form': account_form,
                                                    'contact_form': contact_form,
                                                    'legal_address_form': legal_address_form,
                                                    'title': 'CBF Партенры | Данные для договора'
                                                    })


class SalesView(View):

    template_name = 'sales_page.html'

    def get(self, request, *args, **kwargs):
        if not is_partner(request.user):
            messages.error(request, 'Пожалуйста, авторизируйтесь как партнер чтобы продолжить.')
            return redirect('/partners/login?next=/partners/sales')
        sales = request.user.partner.sales.all() if hasattr(request.user.partner, 'sales') else None
        return render(request, self.template_name, {'sales': sales,
                                                    'partner': request.user.partner,
                                                    'title': 'CBF Партенры | Список продаж'
                                                    })


class ProductsListView(View):

    def get(self, request, *args, **kwargs):
        queryset = Partner.objects.all()
        products = set()
        for partner in queryset:
            products |= set([x.strip() for x in partner.products.split(',') if x != ''])

        return HttpResponse(json.dumps(list(products), ensure_ascii=False),
                            content_type='application/json')
