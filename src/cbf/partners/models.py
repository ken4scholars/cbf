import uuid
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import models
from autoslug import AutoSlugField


class Partner(models.Model):

    NEW, ACTIVATED, DEACTIVATED = 0, 1, 2

    STATUS = (
        (NEW, _('Новый')),
        (ACTIVATED, _('Активирован')),
        (DEACTIVATED, _('Деактивирован'))
    )

    # Fields
    name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(unique=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    payment_data = JSONField(null=True, blank=True)
    products = models.CharField(max_length=10000, null=True, blank=True, default='')
    status = models.SmallIntegerField(choices=STATUS, default=NEW)
    city = models.CharField(max_length=10000, blank=False)
    percent = models.SmallIntegerField(default=10)
    public_percent = models.SmallIntegerField(default=8)
    address = JSONField(default=dict)
    setup = models.BooleanField(default=False)
    phone = models.CharField(max_length=15, null=True, blank=True, default='')
    website = models.CharField(max_length=100, null=True, blank=True, default='')
    balance = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    # cashier_secret = AutoSlugField(unique=True)
    cashier_secret = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    # slug = AutoSlugField(populate_from='name', unique=True)

    # Relationship Fields
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='partner')

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def __str__(self):
        return self.name or self.email

    def get_absolute_url(self):
        return reverse('partners_partner_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('partners_partner_update', args=(self.pk,))

    def delete(self, *args, **kwargs):
        self.user.delete()
        return super(Partner, self).delete(*args, **kwargs)

    @property
    def is_new(self):
        return self.status == self.NEW


class PartnerPayment(models.Model):
    CREATED, PAID, WITHDRAWN, CANCELED = 0, 1, 2, 3

    TRANSACTION_STATUS = (
        (CREATED, _('Создан')),
        (PAID, _('Выплачен')),
        (WITHDRAWN, _('Выведен')),
        (CANCELED, _('Отменен')),
    )

    # Fields
    invoice_number = models.CharField(max_length=100, null=True, unique=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    status = models.SmallIntegerField(choices=TRANSACTION_STATUS, default=CREATED)
    sum = models.DecimalField(max_digits=20, decimal_places=2)
    payment_date = models.DateField(null=True)

    # Relationship Fields
    partner = models.ForeignKey(Partner, related_name='payments')

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('partners_partnerpayment_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('partners_partnerpayment_update', args=(self.pk,))
