import logging
from django.utils import timezone
from datetime import datetime
from cbf.celery import app
from celery.schedules import crontab
from celery.task import task

from cbf.clients.models import Purchase, CREDITED, PAID, WITHDRAWN
from .models import PartnerPayment, Partner


logger = logging.getLogger(__name__)


# @periodic_task(
#     run_every=(crontab(minute='9', hour='13')),
#     name='Расчет отчета партнеров каждые 2 недели',
#     ignore_result=True
# )
# @app.task(name='Расчет отчета партнеров каждые 2 недели')
# def add():
#     logger.info("Tweet download and summarization finished")
#     print("Task Called")


# @periodic_task(
#     run_every=(crontab(minute=19, hour=11, day_of_week='friday')),
#     name='Расчет отчета партнеров',
#     # ignore_result=True
# )
@task()
def make_invoices():
    week_number = int(datetime.today().strftime("%U"))
    if week_number % 2 == 0:
        return
    logger.info("Starting invoices creation task...")
    partners = Partner.objects.all()

    try:
        for partner in partners:
            invoices = PartnerPayment.objects.filter(partner=partner, status__in=[PartnerPayment.CREATED,
                                                                                  PartnerPayment.PAID,
                                                                                  PartnerPayment.WITHDRAWN])
            sales = Purchase.objects.filter(partner=partner, status__in=[CREDITED, PAID, WITHDRAWN])
            invoices_sum = sum([invoice.sum for invoice in invoices])
            sales_sum = sum([sale.sum for sale in sales])
            new = PartnerPayment.objects.create(sum=sales_sum-invoices_sum, partner=partner)
            new.invoice_number = make_number(partner, new.id)
            new.save()
            partner.balance = 0
            partner.save()
        logger.info("New invoices successfully created")
    except Exception as e:
        logger.error("Error while calculating new invoices: {}".format(e))


def make_number(partner, invoice_id):
    return '{}-{}-{}'.format(partner.id, timezone.now().date(), invoice_id)