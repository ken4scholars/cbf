from django.contrib import admin
from django import forms
from django.contrib.postgres.forms.jsonb import InvalidJSONInput, JSONField
from cbf.core.fields import UTF8JSONFormField
from .models import Partner, PartnerPayment


class PartnerAdminForm(forms.ModelForm):
    payment_data = UTF8JSONFormField(required=False)
    address = UTF8JSONFormField()

    class Meta:
        model = Partner
        fields = '__all__'


class PartnerAdmin(admin.ModelAdmin):
    form = PartnerAdminForm
    list_display = ['name', 'created', 'last_updated', 'status', 'city',
                    'percent', 'public_percent', 'phone', 'website', 'balance']
    readonly_fields = []

admin.site.register(Partner, PartnerAdmin)


class PartnerPaymentsAdminForm(forms.ModelForm):

    class Meta:
        model = PartnerPayment
        fields = '__all__'


class PartnerPaymentsAdmin(admin.ModelAdmin):
    form = PartnerPaymentsAdminForm
    list_display = ['invoice_number', 'created', 'last_updated', 'status', 'sum', 'payment_date']
    readonly_fields = []

admin.site.register(PartnerPayment, PartnerPaymentsAdmin)
