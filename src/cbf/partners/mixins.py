from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property
from .models import Partner


class PartnerMixin:
    partner_queryset = Partner.objects.all()

    lookup_partner_field = 'pk'
    partner_url_kwarg = 'parner_pk'

    def get_partner_queryset(self):
        return self.partner_queryset.all()

    @cached_property
    def partner(self):
        queryset = self.get_partner_queryset()

        filter_kwargs = {
            self.lookup_partner_field: self.kwargs[self.partner_url_kwarg],
        }
        return get_object_or_404(queryset, **filter_kwargs)
