from django.conf.urls import url
from django.contrib.auth.views import logout_then_login
from . import views


urlpatterns = [
    # url(r'^auth', views.auth, name='auth'),
    url(r'^login$', views.LoginView.as_view(), name='login'),
    url(r"^logout$", logout_then_login, name="logout"),
    url(r'^register$', views.RegisterView.as_view(), name='register'),
    url(r'^(?P<code>[0-9a-f-]+)/change-pass', views.ChangePasswordView.as_view(), name='change_password'),
    url(r'forgot-pass', views.ForgotPasswordView.as_view(), name='forgot_password'),
    url(r'^setup$', views.SetupView.as_view(), name='setup'),
    url(r'^control-panel$', views.ControlPanelView.as_view(), name='control_panel'),
    url(r'^products-list$', views.ProductsListView.as_view(), name='products_list'),
    url(r'^payment-data$', views.PaymentDataView.as_view(), name='payment_data'),
    url(r'^sales$', views.SalesView.as_view(), name='sales'),
    url(r'^(?P<partner_id>\d+)/payment/(?P<invoice_number>\d+)', views.DownloadInvoiceView.as_view(),
        name='download_invoice')
]
