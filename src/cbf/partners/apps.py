from django.apps import AppConfig


class PartnersConfig(AppConfig):
    name = 'cbf.partners'
    label = 'partners'
