# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-05 10:42
from __future__ import unicode_literals

import autoslug.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0008_auto_20170805_1036'),
    ]

    operations = [
        migrations.AddField(
            model_name='partner',
            name='slug',
            field=autoslug.fields.AutoSlugField(default='partner2', editable=False, populate_from=models.CharField(blank=True, default='>20O :><?0=8O', max_length=100, null=True), unique=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='partner',
            name='cashier_secret',
            field=autoslug.fields.AutoSlugField(unique=True),
        ),
    ]
