# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-05 10:36
from __future__ import unicode_literals

import autoslug.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0007_partner_setup'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='cashier_secret',
            field=autoslug.fields.AutoSlugField(default='partner1', editable=False, populate_from='name', unique=True),
            preserve_default=False,
        ),
    ]
