# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-11 00:07
from __future__ import unicode_literals

import autoslug.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Partner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='>20O :><?0=8O', max_length=100, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('payment_data', models.TextField(blank=True, default='', max_length=100000, null=True)),
                ('products', models.CharField(blank=True, default='', max_length=10000, null=True)),
                ('status', models.BooleanField(default=False)),
                ('city', models.CharField(default='070=L', max_length=10000)),
                ('percent', models.SmallIntegerField(default=10)),
                ('public_percent', models.SmallIntegerField(default=8)),
                ('address', models.TextField(blank=True, default='', max_length=10000, null=True)),
                ('phone', models.CharField(blank=True, default='', max_length=15, null=True)),
                ('website', models.CharField(blank=True, default='', max_length=100, null=True)),
                ('balance', models.FloatField(default=0)),
                ('cashier_secret', autoslug.fields.AutoSlugField(null=True, unique=True)),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
        migrations.CreateModel(
            name='PartnerPayment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('invoice_number', models.CharField(blank=True, default='0', max_length=100, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('status', models.SmallIntegerField(default=0)),
                ('sum', models.FloatField()),
                ('payment_date', models.DateField(null=True)),
                ('partner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='partners.Partner')),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
    ]
