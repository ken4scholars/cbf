from django.contrib import admin
from django import forms
from django.contrib.auth import get_user_model
from .models import Client, Purchase, Withdrawal, FriendCashBackTransaction, YandexCashPersonalData, Token


class ClientAdminForm(forms.ModelForm):

    class Meta:
        model = Client
        fields = '__all__'


class ClientAdmin(admin.ModelAdmin):
    form = ClientAdminForm
    list_display = ['user', 'created', 'last_updated', 'gender', 'city',
                    'code', 'cash_balance', 'validation_code', 'cash_total_savings']
    readonly_fields = []

admin.site.register(Client, ClientAdmin)


class PurchaseAdminForm(forms.ModelForm):

    class Meta:
        model = Purchase
        fields = '__all__'


class PurchaseAdmin(admin.ModelAdmin):
    form = PurchaseAdminForm
    list_display = ['client', 'partner', 'created', 'last_updated', 'status', 'sum', 'payment_time', 'category']
    readonly_fields = []

admin.site.register(Purchase, PurchaseAdmin)


class WithdrawalsAdminForm(forms.ModelForm):

    class Meta:
        model = Withdrawal
        fields = '__all__'


class WithdrawalsAdmin(admin.ModelAdmin):
    form = WithdrawalsAdminForm
    list_display = ['client', 'created', 'last_updated', 'payment_time', 'sum', 'final_sum', 'status']
    readonly_fields = []

admin.site.register(Withdrawal, WithdrawalsAdmin)


class FriendCashBackTransactionsAdminForm(forms.ModelForm):

    class Meta:
        model = FriendCashBackTransaction
        fields = '__all__'


class FriendCashBackTransactionsAdmin(admin.ModelAdmin):
    form = FriendCashBackTransactionsAdminForm
    list_display = ['sender', 'receiver', 'created', 'last_updated', 'sum', 'status', 'payment_time']
    readonly_fields = []

admin.site.register(FriendCashBackTransaction, FriendCashBackTransactionsAdmin)


class YandexCashPersonalDataAdminForm(forms.ModelForm):

    class Meta:
        model = YandexCashPersonalData
        fields = '__all__'


class YandexCashPersonalDataAdmin(admin.ModelAdmin):
    form = YandexCashPersonalDataAdminForm
    list_display = ['last_updated', 'skr_destination_card_synonym', 'skr_destination_card_panmask', 'pdr_last_name',
                    'pdr_first_name', 'pdr_middle_name', 'pdr_doc_number', 'pdr_doc_issue_year',
                    'pdr_doc_issue_month', 'pdr_doc_issue_day', 'pdr_birthday', 'pdr_birth_place', 'pdr_doc_issued_by',
                    'pdr_city', 'pdr_address', 'pdr_postcode']
    readonly_fields = []

admin.site.register(YandexCashPersonalData, YandexCashPersonalDataAdmin)


class TokensAdminForm(forms.ModelForm):

    class Meta:
        model = Token
        fields = '__all__'


class TokensAdmin(admin.ModelAdmin):
    form = TokensAdminForm
    list_display = ['created', 'key']
    readonly_fields = []

admin.site.register(Token, TokensAdmin)
