import os
import binascii
import random
import math

from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.db import IntegrityError
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from cbf.partners.models import Partner


MALE, FEMALE = 'm', 'f'
CREDITED, PAID, WITHDRAWN, CANCELED = 0, 1, 2, 3

TRANSACTION_STATUS = (
    (CREDITED, _('Начислено')),
    (PAID, _('Перечислено')),
    (WITHDRAWN, _('Выплачено')),
    (CANCELED, _('Отменено')),
)

# UNPAID, PAID = 0, 1
#
# PAID_STATUS = (
#     (UNPAID, _('Перечислено')),
#     (PAID, _('Не Перечислено')),
# )

GENDER = (
    (MALE, _('Мужской')),
    (FEMALE, _('Женский'))
)


class Client(models.Model):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    gender = models.CharField(max_length=1, choices=GENDER, default=MALE)
    city = models.CharField(max_length=50, blank=True)
    code = models.CharField(max_length=8, unique=True)
    is_registered = models.BooleanField(default=False)
    cash_balance = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    validation_code = models.CharField(max_length=6)
    cash_total_savings = models.FloatField(default=0)

    # Relationship Fields
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, related_name='client')
    invited_by = models.ForeignKey('self', null=True)

    def save(self, *args, **kwargs):
        if not self.validation_code:
            self.generate_validation_code()
        if not self.code:
            return self.generate_code(*args, **kwargs)
        return super(Client, self).save(*args, **kwargs)

    def generate_validation_code(self):
        self.validation_code = random.randint(100000, 999999)

    def generate_code(self, *args, **kwargs):
        try:
            self.code = random.randint(10000000, 99999999)
            return super(Client, self).save(*args, **kwargs)
        except IntegrityError:
            self.generate_code(*args, **kwargs)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def __str__(self):
        if self.user.first_name and self.user.last_name:
            return '{} {}'.format(self.user.first_name, self.user.last_name)
        return self.user.phone

    def get_absolute_url(self):
        return reverse('clients_client_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('clients_client_update', args=(self.pk,))

    def delete(self, *args, **kwargs):
        self.user.delete()
        return super(Client, self).delete(*args, **kwargs)


class Withdrawal(models.Model):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    payment_time = models.DateTimeField(null=True, blank=True)
    sum = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    result = models.TextField(null=True, blank=True)
    final_sum = models.FloatField()
    status = models.SmallIntegerField(choices=TRANSACTION_STATUS, default=CREDITED)

    # Relationship Fields
    client = models.ForeignKey(Client, related_name='withdrawals')

    class Meta:
        ordering = ('-created',)

    def save(self, *args, **kwargs):
        if not self.final_sum:
            percentage = getattr(settings, 'YANDEX_PERCENTAGE_COMMISSION', 2.8)
            static = getattr(settings, 'YANDEX_STATIC_COMMISSION', 30)
            self.final_sum = self.sum - math.ceil(percentage * self.sum + static)
        super(Withdrawal, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % self.pk

    def __str__(self):
        return 'Вывод средств {}:{}'.format(self.client, self.sum)

    def get_absolute_url(self):
        return reverse('clients_withdrawal_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('clients_withdrawal_update', args=(self.pk,))


class Purchase(models.Model):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    status = models.SmallIntegerField(choices=TRANSACTION_STATUS, default=CREDITED)
    sum = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    # paid_status = models.BooleanField(default=UNPAID)
    payment_time = models.DateTimeField(null=True, blank=True)
    category = models.CharField(max_length=100)

    # Relationship Fields
    client = models.ForeignKey(Client, related_name='purchases')
    partner = models.ForeignKey(Partner, related_name='sales')
    withdrawal = models.ForeignKey(Withdrawal, related_name='purchases', null=True, blank=True)

    class Meta:
        ordering = ('-created',)

    # def save(self, *args, **kwargs):
    #     if self.paid_status is UNPAID and self.status is PAID:
    #         self.client.cash_balance += self.sum
    #         self.client.cash_total_savings += self.sum
    #         self.paid_status = PAID
    #     super(Purchase, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % self.pk

    def __str__(self):
        return '{}-{}'.format(self.category, self.sum)

    def get_absolute_url(self):
        return reverse('clients_purchase_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('clients_purchase_update', args=(self.pk,))

    @property
    def is_deletable(self):
        return timezone.now() - self.created < timezone.timedelta(hours=1)


class FriendCashBackTransaction(models.Model):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    sum = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    # paid_status = models.BooleanField(default=UNPAID)
    status = models.SmallIntegerField(choices=TRANSACTION_STATUS, default=CREDITED)
    payment_time = models.DateTimeField()

    # Relationship Fields
    sender = models.ForeignKey(Client, related_name='sent_transactions', on_delete=models.CASCADE)
    receiver = models.ForeignKey(Client, related_name='received_transactions', on_delete=models.CASCADE)

    class Meta:
        ordering = ('-created',)

    # def save(self, *args, **kwargs):
    #     if self.paid_status is UNPAID and self.status is PAID:
    #         self.client.cash_balance += self.sum
    #         self.client.cash_total_savings += self.sum

    def __unicode__(self):
        return u'%s' % self.pk

    def __str__(self):
        return 'Покупатель-{} Получатель-{}'.format(self.sender, self.receiver)

    def get_absolute_url(self):
        return reverse('clients_friendcashbacktransaction_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('clients_friendcashbacktransaction_update', args=(self.pk,))


class YandexCashPersonalData(models.Model):

    # Fields
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    skr_destination_card_synonym = models.CharField(max_length=50, null=True, blank=True)
    skr_destination_card_panmask = models.CharField(max_length=30, null=True, blank=True)
    pdr_last_name = models.CharField(max_length=50)
    pdr_first_name = models.CharField(max_length=50)
    pdr_middle_name = models.CharField(max_length=50)
    pdr_doc_number = models.CharField(max_length=10)
    pdr_doc_issue_year = models.CharField(max_length=4)
    pdr_doc_issue_month = models.CharField(max_length=2)
    pdr_doc_issue_day = models.CharField(max_length=2)
    pdr_birthday = models.CharField(max_length=10)
    pdr_birth_place = models.CharField(max_length=65)
    pdr_doc_issued_by = models.CharField(max_length=100)
    pdr_city = models.CharField(max_length=30)
    pdr_address = models.CharField(max_length=100)
    pdr_postcode = models.CharField(max_length=6)

    # Relationship Fields
    client = models.OneToOneField(Client, related_name='yandex_data', on_delete=models.CASCADE)

    class Meta:
        ordering = ('-pk',)

    def __unicode__(self):
        return u'%s' % self.pk

    def __str__(self):
        return 'Данные Карты-{}'.format(self.client)

    def get_absolute_url(self):
        return reverse('clients_yandexcashpersonaldata_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('clients_yandexcashpersonaldata_update', args=(self.pk,))


class SMS(models.Model):
    phone = models.CharField(max_length=10, primary_key=True)
    code = models.IntegerField()

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = self.generate_code()
        return super(SMS, self).save(*args, **kwargs)

    def generate_code(self):
        return random.sample(100000, 999999)


class Token(models.Model):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    key = models.CharField(max_length=40, primary_key=True)

    # Relationship Fields
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)

    class Meta:
        ordering = ('-created',)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(Token, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __unicode__(self):
        return u'%s' % self.pk

    def __str__(self):
        return self.key

    def get_absolute_url(self):
        return reverse('clients_token_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('clients_token_update', args=(self.pk,))
