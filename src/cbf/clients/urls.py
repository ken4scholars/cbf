from django.conf.urls import url
from rest_framework.routers import SimpleRouter

from .views import SendSMSAPIView, ObtainTokenAPIView, \
    ClientViewSet, ClientRegistrationAPIView, ClientAPIView, CityCatalogAPIView, \
    TransactionsAPIView, FriendsAPIView, YandexCashDataAPIView, WithdrawalAPIView, \
    PurchaseAPIView


router = SimpleRouter(trailing_slash=False)
# router.register(r'send_auth_sms', SendSMSViewSet, base_name='sms')
# router.register(r'get_token', ObtainTokenViewSet, base_name='token')
# router.register(r'transactions', TransactionsAPIView, base_name='transaction')
# router.register(r'',  ClientViewSet, base_name='client')

urlpatterns = [
    url(r'^send_auth_sms$', SendSMSAPIView.as_view()),
    url(r'^get_token$', ObtainTokenAPIView.as_view()),
    url(r'^register$', ClientRegistrationAPIView.as_view()),
    url(r'^get_info$', ClientAPIView.as_view()),
    url(r'^get_city_catalog$', CityCatalogAPIView.as_view()),
    url(r'^get_transactions$', TransactionsAPIView.as_view()),
    url(r'^get_friends$', FriendsAPIView.as_view()),
    url(r'^personal_payment_data', YandexCashDataAPIView.as_view()),
    url(r'^withdrawals', WithdrawalAPIView.as_view()),
    url(r'^get_purchases', PurchaseAPIView.as_view()),

]

urlpatterns += router.get_urls()
