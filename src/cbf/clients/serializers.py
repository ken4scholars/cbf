import logging
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model

from cbf.core.fields import PhoneField
from cbf.core.utils import get_object_or_none
from cbf.users.serializers import UserSerializer
from cbf.partners.serializers import PartnerSerializer
from . import models


logger = logging.getLogger(__name__)


class SMSSerializer(serializers.Serializer):
    phone = PhoneField(label='phone')


class ObtainTokenSerializer(SMSSerializer):
    code = serializers.CharField(label='code', max_length=6, min_length=5)

    def validate(self, attrs):
        phone = attrs.get('phone')
        user = get_object_or_none(get_user_model(), phone=phone)
        if user:
            attrs['user'] = user
        else:
            msg = _('Клиент с таким номером телефона не найден')
            raise serializers.ValidationError(msg, code='authorization')
        return super(ObtainTokenSerializer, self).validate(attrs)


class ClientSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True, default=CurrentUserDefault())
    promo_code = serializers.CharField(label='promo_code', required=True, write_only=True, max_length=8, min_length=8)
    first_name = serializers.CharField(label='first_name', required=True, write_only=True)
    last_name = serializers.CharField(label='last_name', required=True, write_only=True)

    class Meta:
        model = models.Client
        fields = ('id', 'user', 'first_name', 'last_name', 'created', 'last_updated',
                  'gender', 'city', 'code', 'cash_balance', 'promo_code',
                  'cash_total_savings', 'invited_by',
                  )

        extra_kwargs = {
            'id': {'read_only': True},
            'code': {'read_only': True},
            'created': {'read_only': True},
            'last_updated': {'read_only': True},
            'cash_total_savings': {'read_only': True},
            'cash_balance': {'read_only': True},
            'invited_by': {'read_only': True},
        }

    def validate(self, attrs):
        promo_code = attrs.get('promo_code')
        client = get_object_or_none(self.Meta.model, code=promo_code)
        if client:
            attrs['invited_by'] = client
        else:
            msg = _('Клиент с таким кодом-{}-не найден'.format(promo_code))
            raise serializers.ValidationError(msg, code='authorization')
        return super(ClientSerializer, self).validate(attrs)

    def create(self, validated_data):
        try:
            user = validated_data['user']
            user.first_name = validated_data['first_name']
            user.last_name = validated_data['last_name']
            user.save()
            instance = models.Client.objects.get(user=validated_data['user'])
            return self.update(instance, validated_data)
        except Exception:
            msg = _('либо не авторизован либо авторизация неправильна выполнена')
            raise serializers.ValidationError(msg, code='authorization')

    def to_representation(self, obj):
        """Move fields from user to client representation."""
        representation = super(ClientSerializer, self).to_representation(obj)
        invited_by = representation.pop('invited_by')
        try:
            inviter = get_object_or_none(models.Client, id=invited_by)
            if inviter:
                representation['invited_by'] = inviter.user.phone
        except Exception as e:
            logger.error('Error: {} \n while trying to get the invited_by for client {}'.format(e, obj))
            representation['invited_by'] = None
        user_representation = representation.pop('user')
        for key in user_representation:
            representation[key] = user_representation[key]

        return representation


class TransactionSerializer(serializers.ModelSerializer):
    sender = ClientSerializer()
    receiver = ClientSerializer()
    
    class Meta:
        model = models.FriendCashBackTransaction
        fields = '__all__'


class YandexCashDataSerializer(serializers.ModelSerializer):
    client = ClientSerializer(required=False)

    class Meta:
        model = models.YandexCashPersonalData
        fields = '__all__'
        read_only_fields = ('last_updated', 'client')

    def validate(self, attrs):
        try:
            client = self.context['request'].user.client
            attrs['client'] = client
        except Exception as e:
            msg = _('либо не авторизован либо авторизация неправильна выполнена: ' + str(e))
            raise serializers.ValidationError(msg, code='authorization')
        return super(YandexCashDataSerializer, self).validate(attrs)


class WithdrawalSerializer(serializers.ModelSerializer):
    client = ClientSerializer(read_only=True)

    class Meta:
        model = models.Withdrawal
        fields = '__all__'


class PurchaseSerializer(serializers.ModelSerializer):
    client = ClientSerializer()
    partner = PartnerSerializer()

    class Meta:
        model = models.Purchase
        fields = '__all__'
