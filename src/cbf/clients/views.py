import logging

from django.contrib.auth import get_user_model
from django.conf import settings
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics
from rest_framework.viewsets import ModelViewSet

from cbf.contrib.smsaero import SmsAeroError
from cbf.core.sms import send_code
from cbf.users import models as user_models
from cbf.partners.models import Partner
from cbf.partners.serializers import PartnerSerializer
from .models import SMS, Client, Token, FriendCashBackTransaction, Purchase, Withdrawal, YandexCashPersonalData
from .serializers import SMSSerializer, ObtainTokenSerializer,\
    ClientSerializer, TransactionSerializer, YandexCashDataSerializer, WithdrawalSerializer, \
    PurchaseSerializer

logger = logging.getLogger(__name__)


class SendSMSAPIView(generics.CreateAPIView):
    serializer_class = SMSSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # print(serializer.validated_data['phone'])
            if serializer.validated_data['phone'] == '+79121546725':
                return Response({'sent': True, 'message': 'Код успешно отправлен'}, status=status.HTTP_200_OK)
            user, _ = get_user_model().objects.get_or_create(phone=serializer.validated_data['phone'])
            if hasattr(user, 'partner'):
                return Response({'sent': False, 'message': 'Этот номер уже привязан к другой учетной записи'},
                                status=status.HTTP_406_NOT_ACCEPTABLE)
            if hasattr(user, 'client'):
                client = user.client
            else:
                client = Client.objects.create(user=user)
            try:
                client.generate_validation_code()
                client.save()
                send_code(phone=serializer.validated_data['phone'], code=user.client.validation_code)
                return Response({'sent': True, 'message': 'Код успешно отправлен'}, status=status.HTTP_200_OK)
            except SmsAeroError as e:
                logger.error('Error from SMSAero: {}'.format(e))
                return Response({'sent': False, 'errors': 'Не удалось отправить СМС. Сообщение об ошибке с сервера: {}'.format(e)},
                                status=status.HTTP_400_BAD_REQUEST)
        data = {'sent': False}
        data.update(serializer.errors)
        return Response(data, status=status.HTTP_400_BAD_REQUEST)


class ObtainTokenAPIView(generics.CreateAPIView):
    serializer_class = ObtainTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data['user']
        try:
            client = user.client
            if serializer.validated_data['code'] == client.validation_code:
                token, _ = Token.objects.get_or_create(user=user)
                return Response({'token': token.key,
                                 'is_registered': client.is_registered
                                 },
                                status=status.HTTP_200_OK)
            else:
                return Response({'errors': 'Неверный код'}, status=status.HTTP_400_BAD_REQUEST)
        except Client.DoesNotExist:
            return Response({'errors': 'Клиент с таким номерос телефона не существует'},
                            status=status.HTTP_404_NOT_FOUND)


class ClientRegistrationAPIView(generics.CreateAPIView):
    serializer_class = ClientSerializer

    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        user.first_name = serializer.validated_data.get('first_name')
        user.last_name = serializer.validated_data.get('last_name')
        user.role = user_models.CLIENT
        client = Client.objects.get(user=user)
        if client:
            if client.is_registered:
                return Response({'errors': 'Клиент уже регистрирован'}, status=status.HTTP_406_NOT_ACCEPTABLE)
            if serializer.validated_data.get('invited_by').id != client.id:
                client.invited_by = serializer.validated_data.get('invited_by')
                client.is_registered = True
                client.save()
                serializer.save()
                return Response({'message': 'Клиент успешно зарегистрирован'}, status=status.HTTP_200_OK)
            return Response({'errors': 'Клиент не может сам себя пригласить'}, status=status.HTTP_403_FORBIDDEN)
        return Response({'errors': 'Клиент с таким номером телефона не существует'},
                        status=status.HTTP_404_NOT_FOUND)


class ClientViewSet(ClientRegistrationAPIView, ModelViewSet):

    queryset = Client.objects.all()


class ClientAPIView(generics.RetrieveAPIView):

    permission_classes = (IsAuthenticated, )
    serializer_class = ClientSerializer

    def get_object(self):
        return self.request.user.client


class CityCatalogAPIView(generics.ListAPIView):

    queryset = Partner.objects.all()
    permission_classes = (IsAuthenticated, )
    serializer_class = PartnerSerializer

    def get_queryset(self):
        return super(CityCatalogAPIView, self).get_queryset()\
            .filter(city=self.request.user.client.city)


class TransactionsAPIView(generics.ListAPIView):

    queryset = FriendCashBackTransaction.objects.all()
    permission_classes = (IsAuthenticated, )
    serializer_class = TransactionSerializer

    def get(self, request, *args, **kwargs):
        # pass
        context = {'request': request}
        purchase_serializer = PurchaseSerializer(self.get_purchases(), many=True, context=context)
        withdrawal_serializer = WithdrawalSerializer(self.get_withdrawals(), many=True, context=context)
        transaction_serializer = TransactionSerializer(self.get_transactions(), many=True, context=context)

        return Response({
            'purchases': purchase_serializer.data,
            'withdrawals': withdrawal_serializer.data,
            'cash_back_transactions': transaction_serializer.data
        }, status=status.HTTP_200_OK)

    def get_purchases(self):
        return Purchase.objects.filter(Q(client=self.request.user.client) |
                                       Q(client__invited_by=self.request.user.client)
                                       )

    def get_withdrawals(self):
        return Withdrawal.objects.filter(Q(client=self.request.user.client) |
                                         Q(client__invited_by=self.request.user.client)
                                         )

    def get_transactions(self):
        return FriendCashBackTransaction.objects.filter(Q(receiver=self.request.user.client) |
                                                        Q(sender=self.request.user.client)
                                                        )


class FriendsAPIView(generics.ListAPIView):
    queryset = Client.objects.all()
    permission_classes = (IsAuthenticated, )
    serializer_class = ClientSerializer

    def get_queryset(self):
        return super(FriendsAPIView, self).get_queryset()\
            .filter(invited_by=self.request.user.client)


class YandexCashDataAPIView(mixins.CreateModelMixin,
                            mixins.UpdateModelMixin,
                            generics.GenericAPIView):

    queryset = YandexCashPersonalData.objects.all()
    serializer_class = YandexCashDataSerializer
    permission_classes = (IsAuthenticated, )

    def post(self, request, *args, **kwargs):
        try:
            YandexCashPersonalData.objects.get(client=request.user.client)
            return Response({'errors': 'данные карты для этого клиента уже существует, '
                                       'используйте метод PATCH чтобы редактировать данные'},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)
        except YandexCashPersonalData.DoesNotExist:
            return self.create(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        self.kwargs[self.lookup_field] = request.user.client.yandex_data.pk
        return self.partial_update(request, *args, **kwargs)


class WithdrawalAPIView(generics.ListCreateAPIView):

    serializer_class = WithdrawalSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Withdrawal.objects.all()

    def get_queryset(self):
        return super(WithdrawalAPIView, self).get_queryset().filter(client=self.request.user.client)

    def post(self, request, *args, **kwargs):
        client = request.user.client
        min_amount = getattr(settings, 'CASHBACK_MIN_AMOUNT', 200)
        if client.cash_balance < min_amount:
            return Response({'request_status': False,
                             'errors': 'Не хваетет баланс на счете для вывода.'
                                       ' Минимум для вывода - {} рублей'.format(min_amount)},
                            status=status.HTTP_412_PRECONDITION_FAILED)
        Withdrawal.objects.create(sum=client.cash_balance, client=client)
        # client.cash_total_savings += client.cash_balance
        client.cash_balance = 0
        client.save()
        return Response({'request_status': True}, status=status.HTTP_200_OK)


class PurchaseAPIView(generics.ListAPIView):

    serializer_class = PurchaseSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Purchase.objects.all()

    def get_queryset(self):
        return super(PurchaseAPIView, self).get_queryset().filter((Q(client=self.request.user.client) |
                                                                   Q(client__invited_by=self.request.user.client)
                                                                   ))
