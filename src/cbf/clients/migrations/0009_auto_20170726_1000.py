# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-26 10:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0008_auto_20170724_2123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friendcashbacktransaction',
            name='receiver',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='received_transactions', to='clients.Client'),
        ),
        migrations.AlterField(
            model_name='friendcashbacktransaction',
            name='sender',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sent_transactions', to='clients.Client'),
        ),
        migrations.AlterField(
            model_name='purchase',
            name='client',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='purchases', to='clients.Client'),
        ),
        migrations.AlterField(
            model_name='purchase',
            name='partner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='purchases', to='partners.Partner'),
        ),
        migrations.AlterField(
            model_name='withdrawal',
            name='client',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='withdrawals', to='clients.Client'),
        ),
        migrations.AlterField(
            model_name='yandexcashpersonaldata',
            name='client',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='yandex_data', to='clients.Client'),
        ),
    ]
