# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-17 09:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0016_auto_20170817_1235'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchase',
            name='payment_time',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
