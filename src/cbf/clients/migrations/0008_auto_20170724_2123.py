# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-24 21:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0007_remove_client_phone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='code',
            field=models.CharField(max_length=8, unique=True),
        ),
    ]
