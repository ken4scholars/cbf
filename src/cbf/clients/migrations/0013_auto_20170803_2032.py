# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-03 20:32
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0012_auto_20170803_1858'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='friendcashbacktransaction',
            name='paid_status',
        ),
        migrations.RemoveField(
            model_name='purchase',
            name='paid_status',
        ),
    ]
