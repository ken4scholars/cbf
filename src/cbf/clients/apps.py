from django.apps import AppConfig


class ClientsConfig(AppConfig):
    name = 'cbf.clients'
    label = 'clients'
