from rest_framework import serializers
from django.contrib.auth import get_user_model

from cbf.core.fields import PhoneField


class UserSerializer(serializers.ModelSerializer):
    phone = PhoneField(label='phone')

    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'phone')
