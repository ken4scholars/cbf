import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _

from cbf.core.validators import validate_phone

CLIENT, PARTNER, SELLER, ADMIN = 'Client', 'Partner', 'Seller', 'Admin'

ROLES = (
    (CLIENT, _('Клиент')),
    (PARTNER, _('Партенер')),
    (SELLER, _('Продавец')),
    (ADMIN, _('Админ'))
)


class User(AbstractUser):
    username = models.CharField(max_length=150, unique=True)
    # phone = models.CharField(validators=[validate_phone], max_length=10, unique=True)
    phone = models.CharField(max_length=12, unique=True)
    role = models.CharField(choices=ROLES, max_length=10, default=ADMIN)

    REQUIRED_FIELDS = []

    def save(self, *args, **kwargs):
        if not self.username and 'username'not in kwargs:
            if 'phone' in kwargs:
                kwargs['username'] = kwargs['phone']
            elif self.phone:
                self.username = self.phone
        super(User, self).save(*args, **kwargs)

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'
        db_table = 'auth_user'

    def __str__(self):
        return self.phone
