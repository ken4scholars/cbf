from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'cbf.users'
    label = 'users'
