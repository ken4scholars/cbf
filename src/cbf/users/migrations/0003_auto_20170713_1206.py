# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-13 12:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20170711_1202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='username',
            field=models.CharField(blank=True, max_length=150, verbose_name='username'),
        ),
    ]
