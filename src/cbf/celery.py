from __future__ import absolute_import, unicode_literals

import os
from celery import Celery
from celery.schedules import crontab
# from cbf.partners.tasks import make_invoices
from django.conf import settings


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cbf.settings')
app = Celery('cbf')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

# app.conf.beat_schedule = {
#     '2 weeks': {
#             'task': 'cbf.partners.tasks.make_invoice',
#             'schedule': crontab(minute=51, hour=11, day_of_week='friday'),
#         }
# }


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))