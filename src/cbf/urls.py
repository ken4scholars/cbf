from django.conf.urls import include, url
from django.contrib import admin

from cbf.clients.urls import urlpatterns as client_urlpatterns
from cbf.cashier.urls import urlpatterns as cashier_urlpatterns
from cbf.partners.urls import urlpatterns as partner_urlpatterns
from cbf.partners.views import BaseView
# admin.autodiscover()


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^clients/', include(client_urlpatterns, namespace='clients')),
    url(r'^cashier/', include(cashier_urlpatterns, namespace='cashier')),
    url(r'^partners/', include(partner_urlpatterns, namespace='partners')),
    url(r'^partners', BaseView.as_view(), name='partners'),
    # url(r'^', BaseView.as_view(), name='base_view')
]
