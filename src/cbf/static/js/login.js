/**
 * Created by kenneth on 04.08.17.
 */
// $('.form').find('input, textarea').on('keyup blur focus', function (e) {
//
//   var $this = $(this),
//       label = $this.prev('label');
//
// 	  if (e.type === 'keyup') {
// 			if ($this.val() === '') {
//           label.removeClass('active highlight');
//         } else {
//           label.addClass('active highlight');
//         }
//     } else if (e.type === 'blur') {
//     	if( $this.val() === '' ) {
//     		label.removeClass('active highlight');
// 			} else {
// 		    label.removeClass('highlight');
// 			}
//     } else if (e.type === 'focus') {
//
//       if( $this.val() === '' ) {
//     		label.removeClass('highlight');
// 			}
//       else if( $this.val() !== '' ) {
// 		    label.addClass('highlight');
// 			}
//     }
//
// });
//
// $('.tab a').on('click', function (e) {
//
//   e.preventDefault();
//
//   $(this).parent().addClass('active');
//   $(this).parent().siblings().removeClass('active');
//
//   target = $(this).attr('href');
//
//   $('.tab-content > div').not(target).hide();
//
//   $(target).fadeIn(600);
//
// });

function cloneMore(selector, type) {
    var newElement = $(selector).clone(true);
    var total = $('#id_' + type + '-TOTAL_FORMS').val();
    newElement.find(':input').each(function() {
        var name = $(this).attr('name').replace('-' + (total-1) + '-','-' + total + '-');
        var id = 'id_' + name;
        $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
    });
    newElement.find('label').each(function() {
        var newFor = $(this).attr('for').replace('-' + (total-1) + '-','-' + total + '-');
        $(this).attr('for', newFor);
    });
    total++;
    $('#id_' + type + '-TOTAL_FORMS').val(total);
    $(selector).after(newElement);
}