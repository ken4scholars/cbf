import json
from django.http import HttpResponse
from django.shortcuts import render, redirect, Http404
from django.template.loader import render_to_string
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import View, CreateView, DeleteView, DetailView, ListView
# from rest_framework.viewsets import ModelViewSet
#
# from cbf.partners.mixins import PartnerMixin
# from .models import Product
# from .serializers import ProductSerializer

from cbf.core.utils import get_object_or_none
from cbf.clients.models import Purchase, Client
from cbf.partners.models import Partner, PartnerPayment
from . import forms

#
# class ProductViewSet(ModelViewSet, PartnerMixin):
#
#     queryset = Product.objects.all()
#     search_fields = ('name',)
#
#     serializer_class = ProductSerializer
#
#     def get_partner_queryset(self):
#         queryset = self.partner_queryset.filter(
#             pk__in=self.request.user.partner.all()
#         )
#         return queryset
#
#     def get_queryset(self):
#         queryset = super(ProductViewSet, self).get_queryset()
#         return queryset.filter(partner=self.partner)
#
#     def perform_create(self, serializer):
#         serializer.save(partner=self.partner)


# @method_decorator(login_required, name='dispatch')


def make_data(request, html=None, new_form=None):
    messages_string = render_to_string('messages.html', request=request)
    data = dict()
    if html:
        data['html'] = html
    if new_form:
        data['new_form'] = new_form
    data['messages'] = messages_string
    return json.dumps(data)


class AddSaleView(View):

    template_name = 'cashier_page.html'
    model = Purchase
    queryset = Purchase.objects.all()
    partner = None

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'title': 'CBF Кассир | Добавление продажа',
                                                    'add_sale_form': forms.SaleForm(),
                                                    'cashier_secret': kwargs['slug'],
                                                    'sales': self.get_queryset(),
                                                    'product_tags': self.get_products(),
                                                    })

    def get_queryset(self):
        partner = self.get_partner()
        if not partner:
            raise Http404
        self.partner = partner
        return self.queryset.filter(partner=partner)

    def get_products(self):
        if not self.partner:
            return None
        products = set([x.strip() for x in self.partner.products.split(',') if x != ''])
        return json.dumps(list(products), ensure_ascii=False)

    def get_partner(self):
        return get_object_or_none(Partner, cashier_secret=self.kwargs['slug'])

    def post(self, request, *args, **kwargs):
        form = forms.SaleForm(request.POST)
        if form.is_valid():
            client = get_object_or_none(Client, code=form.cleaned_data.get('client_code'))
            category = form.cleaned_data.get('category')
            sum = form.cleaned_data.get('sum')
            partner = self.get_partner()
            if not partner:
                messages.error(request, 'Что-то пошло не так. Попробуйте еще раз')
                # form.add_error(None, 'Что-то пошло не так. Попробуйте еще раз')
            else:
                purchase = Purchase.objects.create(sum=sum, category=category, client=client, partner=partner)
                partner.balance -= sum
                partner.save()
                # messages.success(request, 'Продажа успешно добавлена')
                html = render_to_string('sales_row.html', context={'purchase': purchase}, request=request)
                new_form = render_to_string('add_sale_form.html',
                                            request=request,
                                            context={'add_sale_form': forms.SaleForm(),
                                                     'cashier_secret': kwargs['slug']
                                                     })
                return HttpResponse(make_data(request, html, new_form), status=200)
        html = render_to_string('add_sale_form.html', request=request, context={'add_sale_form': form,
                                                                                'cashier_secret': kwargs['slug']
                                                                                })
        return HttpResponse(make_data(request, html), status=400)


class DeleteSaleView(View):

    def delete(self, request, purchase_id, *args, **kwargs):
        purchase = get_object_or_none(Purchase, id=purchase_id)
        if purchase and purchase.partner == self.get_partner():
            purchase.delete()
            # messages.success(request, 'Продажа успешно удалена')
            return HttpResponse(make_data(request), status=200)
        else:
            messages.error(request, 'Вы не имеете права на удаление продажи.')
            return HttpResponse(make_data(request), status=400)

    def get_partner(self):
        return get_object_or_none(Partner, cashier_secret=self.kwargs['slug'])




