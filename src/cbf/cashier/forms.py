from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from cbf.core.utils import get_object_or_none
from cbf.clients.models import Client


UserModel = get_user_model()


class SaleForm(forms.Form):
    category = forms.CharField(label=_('Категория'),
                               required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Категория',
                                                             'class': 'form-control',
                                                             'data-role': 'typeahead',
                                                             }))
    client_code = forms.IntegerField(label=_('Код клиента'),
                                     required=True,
                                     widget=forms.NumberInput(attrs={'placeholder': 'Код клиента',
                                                                     'class': 'form-control',
                                                                     }))

    sum = forms.FloatField(label=_('Сумма'),
                           required=True,
                           widget=forms.NumberInput(attrs={'placeholder': 'Сумма (в Рублях)',
                                                           'class': 'form-control'
                                                           }))

    def clean_client_code(self):
        code = self.cleaned_data.get('client_code')
        client = get_object_or_none(Client, code=code)
        if not client:
            raise forms.ValidationError('Клиент с таким кодом не сущестествует!', 'invalid')
        return code
