from rest_framework import serializers


from .models import Product


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('name', 'description', 'added', 'cost',  'sold')
        extra_kwargs = {
            'added': {'read_only': True},
            'description': {'required': False},
            'sold': {'required': False},
        }
