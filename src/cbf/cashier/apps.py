from django.apps import AppConfig


class CashierConfig(AppConfig):
    name = 'cbf.cashier'
    label = 'cashier'
