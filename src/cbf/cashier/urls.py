from django.conf.urls import url
from rest_framework.routers import SimpleRouter

from . import views
# from .views import ProductViewSet
#
# router = SimpleRouter(trailing_slash=False)
#
# router.register(r'partners/(?P<partner_pk>\d+/)/product', ProductViewSet, base_name='partner-products')


urlpatterns = [
    url('(?P<slug>[0-9a-f-]+)/(?P<purchase_id>\d+)', views.DeleteSaleView.as_view(), name='delete_sale'),
    url('(?P<slug>[0-9a-f-]+)', views.AddSaleView.as_view(), name='add_sale'),

]
