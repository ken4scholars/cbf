from django.db import models
from django.utils.translation import ugettext_lazy as _

from cbf.partners.models import Partner


class Seller(models.Model):
    pass


class Product(models.Model):
    name = models.CharField(_('Название'), max_length=100)
    description = models.TextField(_('Описание'), max_length=1000, null=True, blank=True)
    added = models.DateTimeField(_('Дата добавления'), auto_now_add=True)
    cost = models.FloatField(_('Стоимость'))
    sold = models.BooleanField(_('Продан'), default=False)

    partner = models.ForeignKey(Partner)

    class Meta:
        ordering = ('-added', )
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        return self.name
